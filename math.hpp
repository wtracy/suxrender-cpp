#ifndef MATH_HPP
#define MATH_HPP

typedef struct {double x; double y; double z;} Vector3D;

void normalize(Vector3D& v);
Vector3D normalizedCopy(const Vector3D& v);
double dotProduct(const Vector3D& a, const Vector3D& b);
Vector3D sum(const Vector3D& a, const Vector3D& b);
Vector3D difference(const Vector3D& a, const Vector3D& b);
Vector3D scale(const Vector3D& v, double factor);
bool solveQuadratic(double a, double b, double c, double& x0, double& x1);

#endif
