#include <png++/png.hpp>

#include "render.hpp"

int main(int argc, char** argv) {
  png::image<png::rgb_pixel> image;

  image = render<png::image<png::rgb_pixel>, png::rgb_pixel>(640, 480);

  image.write("out.png");
  return 0;
}
