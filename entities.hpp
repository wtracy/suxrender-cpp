#ifndef ENTITIES_HPP
#define ENTITIES_HPP

#include "shapes.hpp"

typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} Color;

typedef struct {
  const Shape* shape;
  Color ambient;
  Color emissive;
} Entity;

#endif
