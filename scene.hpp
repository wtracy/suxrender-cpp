#ifndef SCENE_HPP
#define SCENE_HPP

#include "entities.hpp"

const Sphere s0 = Sphere({-.5, 0., -5.1}, 1.);
const Sphere s1 = Sphere({0.5, 0., -5.}, 1.);
const Plane p = Plane({0., 1., 0.}, {0., -.5, 0.});

const Vector3D ORIGIN = {0., 0., 0.};
const Vector3D LIGHT = normalizedCopy({1., 1., 0.5});

const Color emissive = {80, 40, 40};
const Color ambient = {
  170, 0, 0};

const Color grassAmbient = {0, 64, 0};
const Color grassEmissive = {48, 64, 48};

const Entity SHAPES[] = {
  {&s0, ambient, emissive},
  {&s1, ambient, emissive},
  {&p, grassAmbient, grassEmissive}
};
const double FAR = 1000000.0;

#endif
