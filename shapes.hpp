#ifndef SHAPES_HPP
#define SHAPES_HPP

#include "math.hpp"

class Shape {
  public:
    /* Returns the distance traveled along the given vector to intersect
     * with this object. If there is no intersection, returns a
     * negative value.
     */
    virtual double intersection(const Vector3D& origin, const Vector3D& ray) const = 0;

    /* Writes the surface normal at "point" to "normal". If the point is too
     * far away from the surface of this object, the result is unlikely to be
     * meaningful.
     */
    virtual Vector3D normal(const Vector3D& point) const = 0;
};

class Sphere: public Shape {
  public:
    Sphere(const Vector3D& source, long radius) : center(source), rSquared(radius*radius) {}

    double intersection(const Vector3D& origin, const Vector3D& ray) const override {
      double t0, t1;

      Vector3D l = difference(origin, center);
      double a = dotProduct(ray, ray);
      double b = 2 * dotProduct(ray, l);
      double c = dotProduct(l, l) - rSquared;

      if (!solveQuadratic(a, b, c, t0, t1))
        return -1.;

      if (t0 < 0)
        return t1;
      return t0;
    }

    Vector3D normal(const Vector3D& point) const override {
      Vector3D result = difference(point, center);
      normalize(result);
      return result;
    }
  private:
    Vector3D center;
    double rSquared;
};

class Plane: public Shape {
  public:
    Plane(const Vector3D& normal, const Vector3D& point) : point(point) {
      nVector = normalizedCopy(normal);
    }

    // Based on the implementation from scratchapixel.com
    double intersection(const Vector3D& origin, const Vector3D& ray) const override {
      double denominator = -dotProduct(nVector, ray);

      if (denominator > 1e-6) {
        Vector3D relative = difference(point, origin);
        return -dotProduct(relative, nVector) / denominator;
      }

      return -1.0;
    }

    Vector3D normal(const Vector3D& point) const override {
      return nVector;
    }
  private:
    Vector3D nVector;
    Vector3D point;
};
#endif
