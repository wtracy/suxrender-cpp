#include <cmath>
#include "math.hpp"

void normalize(Vector3D& v) {
  double size = sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
  v.x/=size;
  v.y/=size;
  v.z/=size;
}

Vector3D normalizedCopy(const Vector3D& v) {
  Vector3D result = v;
  normalize(result);
  return result;
}

double dotProduct(const Vector3D& a, const Vector3D& b) {
  return a.x*b.x + a.y*b.y + a.z*b.z;
}

Vector3D sum(const Vector3D& a, const Vector3D& b) {
  return {a.x + b.x, a.y + b.y, a.z + b.z};
}

Vector3D difference(const Vector3D& a, const Vector3D& b) {
  return {a.x - b.x, a.y - b.y, a.z - b.z};
}

Vector3D scale(const Vector3D& v, double factor) {
  return {v.x * factor, v.y * factor, v.z * factor};
}


// Based on the implementation at scratchapixel.com
bool solveQuadratic(double a, double b, double c, double& x0, double& x1) {
  double discr = b * b - 4 * a * c;

  if (discr < 0) 
    return false;
  if (discr == 0) {
    x0 = x1 = - 0.5 * b / a;
  } else {
    double q = (b > 0) ?
            -0.5 * (b + sqrt(discr)) :
            -0.5 * (b - sqrt(discr));
    x0 = q / a;
    x1 = c / q;

    if (x0 > x1) std::swap(x0, x1);
  }

  return true;
}
