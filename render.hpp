#ifndef SUXRENDER_HPP
#define SUXRENDER_HPP

#include <math.h>
#include "scene.hpp"

Color illuminate(const Color& ambient, const Color& emissive, double light) {
  Color result = {
      (unsigned char)(ambient.r * light),
      (unsigned char)(ambient.g * light),
      (unsigned char)(ambient.b * light)};
  result.r += emissive.r;
  result.g += emissive.g;
  result.b += emissive.b;

  return result;
}


const Entity* doTrace(const Vector3D& origin, const Vector3D& direction, double& distance) {
  const Entity* target = nullptr;
  distance = FAR;

  for (const Entity& entity: SHAPES) {
    double thisDistance = entity.shape->intersection(origin, direction);
    if (thisDistance >= 0.0 && (target == nullptr || thisDistance < distance)) {
        target = &entity;
        distance = thisDistance;
    }
  }

  return target;
}

template<class P>
P renderPixel(const Vector3D& origin, const Vector3D& direction) {
  Color result;
  double distance;

  const Entity* target = doTrace(origin, direction, distance);

  if (target != nullptr) {
    Vector3D point = sum(origin, scale(direction, distance));
    Vector3D normal = target->shape->normal(point);
    double shadowDistance;

    normalize(normal);

    const Entity* shadeCaster = doTrace(sum(point, scale(LIGHT, 1e-10)), LIGHT, shadowDistance);

    if (shadeCaster == nullptr) {
      double theta = dotProduct(normal, LIGHT);
      if (theta < 0.0) theta = 0.0;
      result = illuminate(target->ambient, target->emissive, theta);
    } else {
      result = target->emissive;
    }
  } else {
    // If we didn't hit anything, render the sky
    result = {
      0,
      (unsigned char)((direction.y - 1.) * -0.6 * 255),
      (unsigned char)(((direction.y - 1.) * -0.2 + 0.7) * 255)};
  }

  return P(result.r, result.g, result.b);
}

template<class P>
P renderPixel(
    const unsigned int i,     const unsigned int j,
    const unsigned int width, const unsigned int height) {
  double x = (j/(width -1.) - 0.5) * 2./3. * width / height;
  double y = (i/(height-1.) - 0.5) * -2./3.;
  double z = -1.0;
  Vector3D v{x, y, z};
  normalize(v);

  return renderPixel<P>(ORIGIN, v);
}

template<class I, class P>
I render(const unsigned int w, const unsigned int h) {
  I image(w, h);

  for (unsigned int j = 0; j < w; ++j) {
    for (unsigned int i = 0; i < h; ++i) {
      image[i][j] = renderPixel<P>(i, j, w, h);
    }
  }

  return image;
}

#endif
